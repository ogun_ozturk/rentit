package com.esi.rentit.rest;

import com.esi.rentit.RentItApplication;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RentItApplication.class)
@WebAppConfiguration
public class PurchaseOrderControllerTest {

	@Autowired
	private WebApplicationContext wac;
	private MockMvc mockMvc;

	@Autowired
	ObjectMapper mapper;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	@Test
	public void testPOCreation() throws  Exception{

		mockMvc.perform(post("/api/v1/purchase-order")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{ \"plantInventoryEntryId\": 1, \"plantInventoryItemId\": 1, \"rentalPeriod\": { \"endDate\": \"2020-09-06\", \"startDate\": \"2020-05-04\" }}"))
				.andDo(print())
				.andExpect(status().isCreated())
				.andExpect(content().contentType(MediaTypes.HAL_JSON_UTF8_VALUE))
				.andReturn();

	}

}
