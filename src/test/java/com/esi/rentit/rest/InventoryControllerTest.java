package com.esi.rentit.rest;

import com.esi.rentit.RentItApplication;
import com.esi.rentit.inventory.domain.model.PlantInventoryEntry;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RentItApplication.class)
@WebAppConfiguration
public class InventoryControllerTest {

	@Autowired
	private WebApplicationContext wac;
	private MockMvc mockMvc;

	@Autowired
	ObjectMapper mapper;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	@Test
	public void testFetchCatalog() throws  Exception{

		mockMvc.perform(get("/api/v1/inventory"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));

	}

	@Test
	public void testFetchEntry() throws  Exception{

		MvcResult mvcResult = mockMvc.perform(get("/api/v1/inventory/{id}", "1"))
				.andDo(print())
				.andExpect(status().isOk())
				.andReturn();

		PlantInventoryEntry pe = mapper.readValue(mvcResult.getResponse().getContentAsString(), PlantInventoryEntry.class);
		assertThat( pe!=null);

	}

	@Test
	public void testFetchAvailableItems() throws  Exception{

		mockMvc.perform(post("/api/v1/inventory/{id}/available", "1")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{ \"endDate\": \"2020-09-06\", \"startDate\": \"2020-05-04\" }"))
				.andDo(print())
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andReturn();

	}

}
