package com.esi.rentit.po.rest;

import com.esi.rentit.po.application.dto.PurchaseOrderDto;
import com.esi.rentit.po.application.service.PurchaseOrderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/v1/purchase-order")
@RestController
public class PurchaseOrderController {

    private final PurchaseOrderService purchaseOrderService;

    public PurchaseOrderController(PurchaseOrderService purchaseOrderService) {
        this.purchaseOrderService = purchaseOrderService;
    }

    @CrossOrigin
    @GetMapping
    public ResponseEntity<List<PurchaseOrderDto>> fetchAllPurchaseOrders() {
        return new ResponseEntity<>(purchaseOrderService.fetchAll(), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/{id}")
    public ResponseEntity<PurchaseOrderDto> getPurchaseOrder(@PathVariable("id") Long id) {
        return new ResponseEntity<>(purchaseOrderService.getPurchaseOrderById(id), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/customer-orders")
    public ResponseEntity<List<PurchaseOrderDto>> getPurchaseOrdersByCustomerIdentifier(@RequestParam("customerIdentifier") Long customerIdentifier) {
        return new ResponseEntity<>(purchaseOrderService.getPurchaseOrdersByCustomerIdentifier(customerIdentifier), HttpStatus.OK);
    }

    @CrossOrigin
    @PostMapping
    public ResponseEntity<PurchaseOrderDto> createPurchaseOrder(@RequestBody PurchaseOrderDto dto) {
        return new ResponseEntity<>(purchaseOrderService.createPurchaseOrder(dto), HttpStatus.CREATED);
    }

    @CrossOrigin
    @PutMapping("/{id}")
    public ResponseEntity<PurchaseOrderDto> modifyPurchaseOrder(@PathVariable Long id, @RequestBody PurchaseOrderDto dto) {
        return new ResponseEntity<>(purchaseOrderService.updatePurchaseOrder(id, dto), HttpStatus.OK);
    }

    @CrossOrigin
    @PutMapping("/{id}/accept")
    public ResponseEntity<PurchaseOrderDto> acceptPurchaseOrder(@PathVariable Long id) {
        return new ResponseEntity<>(purchaseOrderService.acceptPurchaseOrder(id), HttpStatus.OK);
    }

    @CrossOrigin
    @PutMapping("/{id}/reject")
    public ResponseEntity<PurchaseOrderDto> rejectPurchaseOrder(@PathVariable Long id) {
        return new ResponseEntity<>(purchaseOrderService.rejectPurchaseOrder(id), HttpStatus.OK);
    }

    @CrossOrigin
    @PutMapping("/{id}/cancel")
    public ResponseEntity<PurchaseOrderDto> cancelPurchaseOrder(@PathVariable Long id) {
        return new ResponseEntity<>(purchaseOrderService.cancelPurchaseOrder(id), HttpStatus.OK);
    }

    @CrossOrigin
    @PutMapping("/{id}/dispatch")
    public ResponseEntity<PurchaseOrderDto> dispatchPlant(@PathVariable Long id) {
        return new ResponseEntity<>(purchaseOrderService.dispatchPlant(id), HttpStatus.OK);
    }

    @CrossOrigin
    @PutMapping("/{id}/return")
    public ResponseEntity<PurchaseOrderDto> returnPlant(@PathVariable Long id) {
        return new ResponseEntity<>(purchaseOrderService.returnPlant(id), HttpStatus.OK);
    }

}
