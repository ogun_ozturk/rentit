package com.esi.rentit.po.domain.repository;

import com.esi.rentit.po.domain.model.POStatus;
import com.esi.rentit.po.domain.model.PurchaseOrder;
import com.esi.rentit.user.domain.model.RentItUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PurchaseOrderRepository extends JpaRepository<PurchaseOrder, Long> {

    List<PurchaseOrder> findAllByStatusEquals(POStatus status);
    List<PurchaseOrder> findAllByCustomer(RentItUser user);
}
