package com.esi.rentit.po.domain.model;

public enum POStatus {
    PENDING,
    ACCEPTED,
    REJECTED,
    DISPATCHED,
    DISPATCHED_BEING_REVIEWED,
    RETURNED,
    CANCELLED,
    EXTENTION_PENDING,
    INVOICED,
    CLOSED
}
