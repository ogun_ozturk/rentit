package com.esi.rentit.po.domain.model;

import com.esi.rentit.common.domain.BaseEntity;
import com.esi.rentit.common.domain.BusinessPeriod;
import com.esi.rentit.inventory.domain.model.PlantInventoryItem;
import com.esi.rentit.invoice.domain.model.Invoice;
import com.esi.rentit.user.domain.model.RentItUser;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "purchase_order", indexes = {@Index(name = "po_idx", columnList = "uuid")})
public class PurchaseOrder extends BaseEntity {

    @Column(name = "uuid", nullable = false)
    private UUID uuid;

    @Column(nullable = false)
    private LocalDate issueDate;

    @Column
    private LocalDate shipmentDate;

    @Column
    private LocalDate extendedDate;

    @Embedded
    @Column(nullable = false)
    private BusinessPeriod rentalPeriod;

    @Column(precision = 8, scale = 2, nullable = false)
    private BigDecimal totalPrice;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private POStatus status;

    @Column(nullable = false)
    private RentItUser customer;

    private String resourceLocation;

    @ManyToOne
    @JoinColumn(name = "plant_inventory_item_id", nullable = false)
    private PlantInventoryItem plantInventoryItem;

    public static PurchaseOrder of(PlantInventoryItem plantInventoryItem,
                                   BusinessPeriod rentalPeriod,
                                   BigDecimal totalPrice,
                                   RentItUser customer) {

        PurchaseOrder po = new PurchaseOrder();

        po.uuid = UUID.randomUUID();
        po.issueDate = LocalDate.now();
        po.shipmentDate = rentalPeriod.getStartDate();
        po.rentalPeriod = rentalPeriod;
        po.totalPrice = totalPrice;
        po.plantInventoryItem = plantInventoryItem;
        po.customer = customer;
        po.status = POStatus.PENDING;

        return po;
    }
}
