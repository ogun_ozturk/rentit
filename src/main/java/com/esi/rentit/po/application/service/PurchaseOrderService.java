package com.esi.rentit.po.application.service;

import com.esi.rentit.common.application.exception.BuildITException;
import com.esi.rentit.common.application.exception.NotFoundException;
import com.esi.rentit.common.domain.BusinessPeriod;
import com.esi.rentit.inventory.domain.model.PlantInventoryEntry;
import com.esi.rentit.inventory.domain.model.PlantInventoryItem;
import com.esi.rentit.inventory.domain.repository.PlantInventoryItemRepository;
import com.esi.rentit.invoice.application.dto.InvoiceDto;
import com.esi.rentit.invoice.application.service.InvoiceAssembler;
import com.esi.rentit.invoice.application.service.InvoiceService;
import com.esi.rentit.invoice.domain.model.Invoice;
import com.esi.rentit.po.application.dto.PurchaseOrderDto;
import com.esi.rentit.po.application.exception.ItemNotAvailableException;
import com.esi.rentit.po.domain.model.POStatus;
import com.esi.rentit.po.domain.model.PurchaseOrder;
import com.esi.rentit.po.domain.repository.PurchaseOrderRepository;
import com.esi.rentit.user.domain.model.RentItUser;
import com.esi.rentit.user.domain.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;
import java.util.Optional;

import static com.esi.rentit.common.application.service.BusinessPeriodValidator.validateBusinessPeriod;

@Service
public class PurchaseOrderService {

    private final PlantInventoryItemRepository plantInventoryItemRepository;
    private final PurchaseOrderRepository purchaseOrderRepository;
    private final PurchaseOrderAssembler purchaseOrderAssembler;
    private final UserRepository userRepository;
    private final InvoiceService invoiceService;
    private final InvoiceAssembler invoiceAssembler;

    private final RestTemplate restTemplate;

    @Autowired
    public PurchaseOrderService(PlantInventoryItemRepository plantInventoryItemRepository,
                                PurchaseOrderRepository purchaseOrderRepository,
                                PurchaseOrderAssembler purchaseOrderAssembler,
                                UserRepository userRepository,
                                InvoiceService invoiceService,
                                RestTemplate restTemplate,
                                ObjectMapper objectMapper,
                                InvoiceAssembler invoiceAssembler) {

        this.plantInventoryItemRepository = plantInventoryItemRepository;
        this.purchaseOrderRepository = purchaseOrderRepository;
        this.purchaseOrderAssembler = purchaseOrderAssembler;
        this.userRepository = userRepository;
        this.invoiceService = invoiceService;
        this.restTemplate = restTemplate;
        this.invoiceAssembler = invoiceAssembler;
    }

    public List<PurchaseOrderDto> fetchAll() {
        return purchaseOrderAssembler.toResources(purchaseOrderRepository.findAll());
    }

    public List<PurchaseOrderDto> getPurchaseOrdersByCustomerIdentifier(Long customerIdentifier) {
        Optional<RentItUser> user = userRepository.findById(customerIdentifier);
        if (!user.isPresent()) throw new NotFoundException("Such user does not exist");
        List<PurchaseOrder> purchaseOrders = purchaseOrderRepository.findAllByCustomer(user.get());

        if (purchaseOrders == null || purchaseOrders.isEmpty()) {
            throw new NotFoundException("You have no purchase order yet");
        }

        return purchaseOrderAssembler.toResources(purchaseOrders);
    }

    public PurchaseOrderDto createPurchaseOrder(PurchaseOrderDto dto) {

        RentItUser user = hasValidUser(dto);
        BusinessPeriod period = BusinessPeriod.of(dto.getRentalPeriod().getStartDate(), dto.getRentalPeriod().getEndDate());
        validateBusinessPeriod(period);

        Optional<PlantInventoryItem> optionalItem = plantInventoryItemRepository.findById(dto.getPlantInventoryItemId());
        if (!optionalItem.isPresent()) throw new NotFoundException("Unable to find item");

        PlantInventoryEntry plantInventoryEntry = optionalItem.get().getPlantInventoryEntry();

        BigDecimal totalPrice = BigDecimal.valueOf(period.durationInDays()).multiply(plantInventoryEntry.getPricePerDay());

        PurchaseOrder po = PurchaseOrder.of(
                optionalItem.get(),
                period,
                totalPrice,
                user
        );

        Link self = dto.getLink("self");
        if (self == null) throw new BuildITException("No self link specified by BuildIT", HttpStatus.UNPROCESSABLE_ENTITY);

        po.setResourceLocation(self.getHref());

        if (isItemStillAvailable(po)) {
            purchaseOrderRepository.save(po);

            return purchaseOrderAssembler.toResource(po);
        } else {
            po.setStatus(POStatus.REJECTED);
            purchaseOrderRepository.save(po);

            throw new ItemNotAvailableException("Item is not available at the moment");
        }
    }

    public PurchaseOrderDto updatePurchaseOrder(Long id, PurchaseOrderDto dto) {

        hasValidUser(dto);
        PurchaseOrder po = getPoById(id);

        POStatus current = po.getStatus();
        BusinessPeriod old = po.getRentalPeriod();

        switch (current) {
            case DISPATCHED:
                BusinessPeriod period = BusinessPeriod.of(old.getStartDate(), dto.getRentalPeriod().getEndDate());
                if (!isItemStillAvaulableWithNewPeriod(po, period)) {
                    throw new ItemNotAvailableException("Item not available for this time interval");
                }
                po.setStatus(POStatus.EXTENTION_PENDING);
                po.setExtendedDate(period.getEndDate());
                purchaseOrderRepository.save(po);
        }
        return purchaseOrderAssembler.toResource(po);
    }

    public PurchaseOrderDto getPurchaseOrderById(Long id) {
        return purchaseOrderAssembler.toResource(getPoById(id));
    }

    public PurchaseOrderDto acceptPurchaseOrder(Long id) {
        PurchaseOrder po = getPoById(id);

        ResponseEntity<PurchaseOrderDto> response = null;
        POStatus status = po.getStatus();
        switch (status) {
            case PENDING:
                POStatus newStatus = isItemStillAvailable(po) ? POStatus.ACCEPTED : POStatus.REJECTED;
                po.setStatus(newStatus);
                purchaseOrderRepository.save(po);
                break;
            case DISPATCHED_BEING_REVIEWED:
                po.setStatus(POStatus.DISPATCHED);
                purchaseOrderRepository.save(po);
                break;
            case EXTENTION_PENDING:
                BusinessPeriod period = BusinessPeriod.of(po.getRentalPeriod().getStartDate(), po.getExtendedDate());
                if (!isItemStillAvaulableWithNewPeriod(po, period)) {
                    throw new ItemNotAvailableException("Item not available for this time interval");
                }
                response = makeBuildITRequest(null, po.getResourceLocation(), HttpMethod.GET);
                PurchaseOrderDto dto = response.getBody();
                if (response.getStatusCode() != HttpStatus.OK || dto == null) throw new BuildITException("Cannot find corresponding PHR from buildIT", response.getStatusCode());
                // successfully found PHR from buildIT

                Link accept = dto.getLink("accept");
                if (accept == null) throw new BuildITException("Cannot perform accept in BuildIT", HttpStatus.UNPROCESSABLE_ENTITY);

                po.setRentalPeriod(period);
                po.setStatus(POStatus.DISPATCHED);
                purchaseOrderRepository.save(po);

                response = makeBuildITRequest(null, accept.getHref(), HttpMethod.PUT);
                if (response.getStatusCode() != HttpStatus.OK) throw new BuildITException("Error occured while accept request to BuildIT", response.getStatusCode());
                // accept done successfully
                break;
        }
        return purchaseOrderAssembler.toResource(po);
    }

    public PurchaseOrderDto rejectPurchaseOrder(Long id) {
        PurchaseOrder po = getPoById(id);

        ResponseEntity<PurchaseOrderDto> response = null;
        POStatus status = po.getStatus();
        switch (status) {
            case PENDING:
                po.setStatus(POStatus.REJECTED);
                purchaseOrderRepository.save(po);
                break;
            case DISPATCHED_BEING_REVIEWED:
                po.setStatus(POStatus.CANCELLED);
                purchaseOrderRepository.save(po);
                break;
            case EXTENTION_PENDING:
                response = makeBuildITRequest(null, po.getResourceLocation(), HttpMethod.GET);
                PurchaseOrderDto dto = response.getBody();
                if (response.getStatusCode() != HttpStatus.OK || dto == null) throw new BuildITException("Cannot find corresponding PHR from buildIT", response.getStatusCode());
                // successfully found PHR from buildIT

                Link reject = dto.getLink("reject");
                if (reject == null) throw new BuildITException("Cannot perform reject in BuildIT", HttpStatus.UNPROCESSABLE_ENTITY);

                response = makeBuildITRequest(null, reject.getHref(), HttpMethod.PUT);
                if (response.getStatusCode() != HttpStatus.OK) throw new BuildITException("Error occured while reject request to BuildIT", response.getStatusCode());
                // reject done successfully
                po.setStatus(POStatus.DISPATCHED);
                purchaseOrderRepository.save(po);
                break;
        }
        return purchaseOrderAssembler.toResource(po);
    }

    public PurchaseOrderDto cancelPurchaseOrder(Long id) {
        PurchaseOrder po = getPoById(id);

        POStatus current = po.getStatus();
        if (current == POStatus.PENDING || current == POStatus.ACCEPTED) po.setStatus(POStatus.CANCELLED);
        purchaseOrderRepository.save(po);

        return purchaseOrderAssembler.toResource(po);
    }

    public PurchaseOrderDto dispatchPlant(Long id) {
        PurchaseOrder po = getPoById(id);

        POStatus current = po.getStatus();
        if (current == POStatus.ACCEPTED) {
            po.setStatus(POStatus.DISPATCHED_BEING_REVIEWED);

            ResponseEntity<PurchaseOrderDto> response = makeBuildITRequest(null, po.getResourceLocation(), HttpMethod.GET);
            PurchaseOrderDto dto = response.getBody();
            if (response.getStatusCode() != HttpStatus.OK || dto == null) throw new BuildITException("Cannot find corresponding PHR from buildIT", response.getStatusCode());
            // successfully found PHR from buildIT

            Link dispatch = dto.getLink("dispatch-review");
            if (dispatch == null) throw new BuildITException("Cannot perform dispatch-review in BuildIT", HttpStatus.UNPROCESSABLE_ENTITY);
            response = makeBuildITRequest(null, dispatch.getHref(), HttpMethod.PUT);
            if (response.getStatusCode() != HttpStatus.OK) throw new BuildITException("Error occured while sending dispatch-review request to BuildIT", response.getStatusCode());
            // dispatch-review sent successfully
            purchaseOrderRepository.save(po);
        }

        return purchaseOrderAssembler.toResource(po);
    }

    public PurchaseOrderDto returnPlant(Long id) {
        PurchaseOrder po = getPoById(id);

        POStatus current = po.getStatus();
        if (current == POStatus.DISPATCHED) po.setStatus(POStatus.RETURNED);
        purchaseOrderRepository.save(po);

        return createInvoice(id);
    }

    private PurchaseOrderDto createInvoice(Long id) {
        PurchaseOrder po = getPoById(id);

        POStatus current = po.getStatus();
        if (current == POStatus.RETURNED) {

            Invoice invoice = invoiceService.createInvoiceForPurchaseorder(po);

            InvoiceDto dto = invoiceAssembler.toResource(invoice);
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaTypes.HAL_JSON_UTF8);

            HttpEntity<InvoiceDto> request = new HttpEntity<>(dto, headers);

            RentItUser user = po.getCustomer();
            String destinationLocation = user.getUriToSendInvoice();

            ResponseEntity<InvoiceDto> response = null;
            try {
                 response = restTemplate.exchange(
                        destinationLocation,
                        HttpMethod.POST,
                        request, InvoiceDto.class);
            } catch (RuntimeException e) {
                invoiceService.removeInvoice(invoice);
                throw new BuildITException(e.getMessage(), HttpStatus.NOT_FOUND);
            }

            if (response.getStatusCode() != HttpStatus.CREATED) {
                invoiceService.removeInvoice(invoice);
                throw new BuildITException("BuildIT was unable to process: " + response.getBody(), response.getStatusCode());
            }

            // request was successful
            po.setStatus(POStatus.INVOICED);
            purchaseOrderRepository.save(po);
        }

        return purchaseOrderAssembler.toResource(po);
    }

    private PurchaseOrder getPoById(Long id) {
        Optional<PurchaseOrder> optionalPo = purchaseOrderRepository.findById(id);

        if (!optionalPo.isPresent()) {
            throw new NotFoundException("Unable to find purchase order");
        }

        return optionalPo.get();
    }

    private boolean isItemStillAvailable(PurchaseOrder po) {
        return plantInventoryItemRepository.isAvailable(
                po.getPlantInventoryItem().getId(),
                Date.valueOf(po.getRentalPeriod().getStartDate()),
                Date.valueOf(po.getRentalPeriod().getEndDate())
        );
    }

    private boolean isItemStillAvaulableWithNewPeriod(PurchaseOrder po, BusinessPeriod period) {
        POStatus current = po.getStatus();
        BusinessPeriod old = po.getRentalPeriod();

        po.setRentalPeriod(period);
        po.setStatus(POStatus.CANCELLED);
        purchaseOrderRepository.save(po);

        boolean available = isItemStillAvailable(po);

        po.setRentalPeriod(old);
        po.setStatus(current);
        purchaseOrderRepository.save(po);

        return available;
    }

    private RentItUser hasValidUser(PurchaseOrderDto dto) {
        Optional<RentItUser> user = userRepository.findById(dto.getRentITCustomerId());
        if (!user.isPresent()) throw new NotFoundException("Such user does not exist");
        return user.get();
    }

    private ResponseEntity<PurchaseOrderDto> makeBuildITRequest(PurchaseOrderDto dto, String url, HttpMethod method) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaTypes.HAL_JSON_UTF8);
        return restTemplate.exchange(url, method, new HttpEntity<>(dto, headers), PurchaseOrderDto.class);
    }
}
