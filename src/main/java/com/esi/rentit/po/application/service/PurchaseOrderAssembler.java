package com.esi.rentit.po.application.service;

import com.esi.rentit.common.application.dto.BusinessPeriodDto;
import com.esi.rentit.inventory.domain.model.PlantInventoryEntry;
import com.esi.rentit.po.application.dto.PurchaseOrderDto;
import com.esi.rentit.po.domain.model.POStatus;
import com.esi.rentit.po.domain.model.PurchaseOrder;
import com.esi.rentit.po.rest.PurchaseOrderController;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class PurchaseOrderAssembler extends ResourceAssemblerSupport<PurchaseOrder, PurchaseOrderDto> {

    public PurchaseOrderAssembler() {
        super(PurchaseOrderController.class, PurchaseOrderDto.class);
    }

    @Override
    public PurchaseOrderDto toResource(PurchaseOrder po) {
        PurchaseOrderDto dto = createResourceWithId(po.getId(), po);

        dto.set_id(po.getId());
        dto.setPurchaseOrderUuid(po.getUuid().toString());
        dto.setPlantInventoryItemId(po.getPlantInventoryItem().getId());
        dto.setIssueDate(po.getIssueDate());
        dto.setShipmentDate(po.getShipmentDate());
        dto.setRentalPeriod(BusinessPeriodDto.of(po.getRentalPeriod().getStartDate(), po.getRentalPeriod().getEndDate()));
        dto.setTotalPrice(po.getTotalPrice());
        dto.setStatus(po.getStatus().name());
        PlantInventoryEntry e = po.getPlantInventoryItem().getPlantInventoryEntry();
        dto.setPlantName(e.getName());
        dto.setConstructionSiteId(po.getCustomer().getId());
        dto.setExtendedDate(po.getExtendedDate());

        // Add links to resource
        POStatus status = po.getStatus();
        switch (status) {
            case PENDING:
                dto.add(linkTo(methodOn(PurchaseOrderController.class).cancelPurchaseOrder(dto.get_id())).withRel("cancel"));
                dto.add(linkTo(methodOn(PurchaseOrderController.class).acceptPurchaseOrder(dto.get_id())).withRel("accept"));
                dto.add(linkTo(methodOn(PurchaseOrderController.class).rejectPurchaseOrder(dto.get_id())).withRel("reject"));
                //dto.add(linkTo(methodOn(PurchaseOrderController.class).modifyPurchaseOrder(dto.get_id(), null)).withRel("modify"));
                break;
            case ACCEPTED:
                dto.add(linkTo(methodOn(PurchaseOrderController.class).cancelPurchaseOrder(dto.get_id())).withRel("cancel"));
                dto.add(linkTo(methodOn(PurchaseOrderController.class).dispatchPlant(dto.get_id())).withRel("dispatch"));
                dto.add(linkTo(methodOn(PurchaseOrderController.class).modifyPurchaseOrder(dto.get_id(), null)).withRel("modify"));
                break;
            case DISPATCHED_BEING_REVIEWED:
                dto.add(linkTo(methodOn(PurchaseOrderController.class).acceptPurchaseOrder(dto.get_id())).withRel("accept"));
                dto.add(linkTo(methodOn(PurchaseOrderController.class).rejectPurchaseOrder(dto.get_id())).withRel("reject"));
                break;
            case DISPATCHED:
                dto.add(linkTo(methodOn(PurchaseOrderController.class).returnPlant(dto.get_id())).withRel("return"));
                dto.add(linkTo(methodOn(PurchaseOrderController.class).modifyPurchaseOrder(dto.get_id(), null)).withRel("modify"));
                break;
            case EXTENTION_PENDING:
                dto.add(linkTo(methodOn(PurchaseOrderController.class).acceptPurchaseOrder(dto.get_id())).withRel("accept"));
                dto.add(linkTo(methodOn(PurchaseOrderController.class).rejectPurchaseOrder(dto.get_id())).withRel("reject"));
            case INVOICED:
                // no operations
                break;
        }


        return dto;
    }
}
