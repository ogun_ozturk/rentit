package com.esi.rentit.po.application.dto;

import com.esi.rentit.common.application.dto.BusinessPeriodDto;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.hateoas.ResourceSupport;

import java.math.BigDecimal;
import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor(force = true, access = AccessLevel.PUBLIC)
@AllArgsConstructor(staticName = "of")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PurchaseOrderDto extends ResourceSupport {
    Long _id;
    String purchaseOrderUuid;
    Long plantInventoryItemId;
    LocalDate issueDate;
    String plantName;
    LocalDate shipmentDate;
    BusinessPeriodDto rentalPeriod;
    BigDecimal totalPrice;
    String status;
    Long constructionSiteId;
    Long rentITCustomerId;
    LocalDate extendedDate;
}
