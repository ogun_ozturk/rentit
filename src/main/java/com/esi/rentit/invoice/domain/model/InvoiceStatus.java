package com.esi.rentit.invoice.domain.model;

public enum InvoiceStatus {
    PENDING,
    REJECTED,
    CLOSED
}
