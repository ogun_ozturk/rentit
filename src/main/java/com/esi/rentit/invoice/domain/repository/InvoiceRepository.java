package com.esi.rentit.invoice.domain.repository;

import com.esi.rentit.invoice.domain.model.Invoice;
import com.esi.rentit.invoice.domain.model.InvoiceStatus;
import com.esi.rentit.po.domain.model.PurchaseOrder;
import com.esi.rentit.user.domain.model.RentItUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, Long> {


    List<Invoice> findAllByInvoiceStatusAndPurchaseOrderCustomer(InvoiceStatus status, RentItUser user);
    Optional<Invoice> findByPurchaseOrder(PurchaseOrder po);

}
