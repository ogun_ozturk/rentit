package com.esi.rentit.invoice.domain.model;

import com.esi.rentit.common.domain.BaseEntity;
import com.esi.rentit.po.domain.model.PurchaseOrder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "invoice", indexes = {@Index(name = "invoice_idx", columnList = "uuid")})
public class Invoice extends BaseEntity {

    @Column(name = "uuid", nullable = false)
    private UUID uuid;

    @Column(nullable = false)
    private LocalDate paymentDate;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private InvoiceStatus invoiceStatus;

    @OneToOne(optional = false, fetch = FetchType.EAGER)
    private PurchaseOrder purchaseOrder;

    public static Invoice of(LocalDate paymentDate, PurchaseOrder order) {
        Invoice invoice = new Invoice();
        invoice.uuid = UUID.randomUUID();
        invoice.paymentDate = paymentDate;
        invoice.purchaseOrder = order;
        invoice.invoiceStatus = InvoiceStatus.PENDING;
        return invoice;
    }
}
