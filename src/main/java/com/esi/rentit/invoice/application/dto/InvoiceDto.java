package com.esi.rentit.invoice.application.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.hateoas.ResourceSupport;

import java.math.BigDecimal;
import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class InvoiceDto extends ResourceSupport {

    Long _id;
    String invoiceUuid;
    String plantName;
    String purchaseOrderUuid;
    LocalDate paymentDate;
    String status;
    BigDecimal totalPrice;

}
