package com.esi.rentit.invoice.application.service;

import com.esi.rentit.invoice.application.dto.InvoiceDto;
import com.esi.rentit.invoice.domain.model.Invoice;
import com.esi.rentit.invoice.rest.InvoiceRestController;
import com.esi.rentit.po.domain.model.PurchaseOrder;
import com.esi.rentit.po.rest.PurchaseOrderController;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class InvoiceAssembler extends ResourceAssemblerSupport<Invoice, InvoiceDto> {

    public InvoiceAssembler() {
        super(InvoiceRestController.class, InvoiceDto.class);
    }

    @Override
    public InvoiceDto toResource(Invoice invoice) {
        InvoiceDto dto = createResourceWithId(invoice.getId(), invoice);

        dto.set_id(invoice.getId());
        dto.setInvoiceUuid(invoice.getUuid().toString());

        PurchaseOrder po = invoice.getPurchaseOrder();
        dto.setPurchaseOrderUuid(po.getUuid().toString());
        dto.setPaymentDate(invoice.getPaymentDate());
        dto.setStatus(invoice.getInvoiceStatus().name());
        dto.setTotalPrice(po.getTotalPrice());
        dto.setPlantName(po.getPlantInventoryItem().getPlantInventoryEntry().getName());

        dto.add(linkTo(methodOn(InvoiceRestController.class).acceptInvoice(dto.get_id())).withRel("acceptInvoice"));
        dto.add(linkTo(methodOn(InvoiceRestController.class).rejectInvoice(dto.get_id())).withRel("rejectInvoice"));

        return dto;
    }
}
