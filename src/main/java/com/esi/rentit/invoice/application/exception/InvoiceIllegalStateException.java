package com.esi.rentit.invoice.application.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class InvoiceIllegalStateException extends RuntimeException {
    public InvoiceIllegalStateException(String message) {
        super(message);
    }
}
