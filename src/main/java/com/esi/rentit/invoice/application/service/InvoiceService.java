package com.esi.rentit.invoice.application.service;

import com.esi.rentit.common.application.exception.BuildITException;
import com.esi.rentit.common.application.exception.NotFoundException;
import com.esi.rentit.invoice.application.dto.InvoiceDto;
import com.esi.rentit.invoice.application.exception.ExistingInvoiceException;
import com.esi.rentit.invoice.application.exception.InvoiceIllegalStateException;
import com.esi.rentit.invoice.domain.model.Invoice;
import com.esi.rentit.invoice.domain.model.InvoiceStatus;
import com.esi.rentit.invoice.domain.repository.InvoiceRepository;
import com.esi.rentit.po.domain.model.POStatus;
import com.esi.rentit.po.domain.model.PurchaseOrder;
import com.esi.rentit.po.domain.repository.PurchaseOrderRepository;
import com.esi.rentit.user.domain.model.RentItUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class InvoiceService {

    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    InvoiceAssembler invoiceAssembler;

    @Autowired
    RestTemplate restTemplate;

    public Invoice createInvoiceForPurchaseorder(PurchaseOrder po) {

        Optional<Invoice> existing = invoiceRepository.findByPurchaseOrder(po);
        LocalDate paymentDate = LocalDate.now().plusWeeks(2); // due date is after 2 weeks from now
        Invoice invoice = Invoice.of(paymentDate, po);

        if (existing.isPresent()) {
            InvoiceStatus status = existing.get().getInvoiceStatus();
            if (status != InvoiceStatus.REJECTED) throw new ExistingInvoiceException("There is already one active invoice");
            invoiceRepository.delete(existing.get());

        }
        invoiceRepository.save(invoice);
        return invoice;
    }

    public void removeInvoice(Invoice invoice) {
        invoiceRepository.delete(invoice);
    }

    public List<InvoiceDto> fetchInvoices() {
        List<Invoice> invoices = invoiceRepository.findAll();
        return invoiceAssembler.toResources(invoices);
    }

    public InvoiceDto fetchInvoice(Long id) {
        Optional<Invoice> invoice = invoiceRepository.findById(id);
        if (!invoice.isPresent()) throw new NotFoundException("Invoice with such id was not found");
        return invoiceAssembler.toResource(invoice.get());
    }

    public InvoiceDto accept(Long id) {
        Optional<Invoice> invoice = invoiceRepository.findById(id);
        if(!invoice.isPresent()) throw new NotFoundException("Invoice with such id was not found");
        Invoice inv = invoice.get();
        if (inv.getInvoiceStatus() != InvoiceStatus.PENDING) throw new InvoiceIllegalStateException("Remittance advice cannot be submitted, because invoice is not in PENDING state");
        inv.setInvoiceStatus(InvoiceStatus.CLOSED);
        invoiceRepository.save(inv);

        PurchaseOrder po = inv.getPurchaseOrder();
        po.setStatus(POStatus.CLOSED);
        purchaseOrderRepository.save(po);

        return invoiceAssembler.toResource(inv);
    }

    public InvoiceDto reject(Long id) {
        Optional<Invoice> invoice = invoiceRepository.findById(id);
        if (!invoice.isPresent()) throw new NotFoundException("Invoice with such id was not found");
        Invoice inv = invoice.get();
        if (inv.getInvoiceStatus() != InvoiceStatus.PENDING) throw new InvoiceIllegalStateException("Invoice cannot be rejected, because it is not in PENDING state");
        inv.setInvoiceStatus(InvoiceStatus.REJECTED);
        invoiceRepository.save(inv);

        PurchaseOrder po = inv.getPurchaseOrder();
        po.setStatus(POStatus.RETURNED);
        purchaseOrderRepository.save(po);

        return invoiceAssembler.toResource(inv);
    }

    public InvoiceDto reminder(Long id) {
        Optional<Invoice> invoice = invoiceRepository.findById(id);
        if (!invoice.isPresent()) throw new NotFoundException("Invoice with such id was not found");
        Invoice inv = invoice.get();
        if (inv.getInvoiceStatus() != InvoiceStatus.PENDING) throw new InvoiceIllegalStateException("Invoice must be in PENDING state, when reminder is sent");

        PurchaseOrder po = inv.getPurchaseOrder();
        RentItUser user = po.getCustomer();
        String destinationLocation = user.getUriToSendInvoice();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaTypes.HAL_JSON_UTF8);

        InvoiceDto dto = invoiceAssembler.toResource(inv);
        HttpEntity<InvoiceDto> request = new HttpEntity<>(dto, headers);
        ResponseEntity<InvoiceDto> response = null;
        try {
            response = restTemplate.exchange(
                    destinationLocation,
                    HttpMethod.PUT,
                    request, InvoiceDto.class);
        } catch (RuntimeException e) {
            throw new BuildITException(e.getMessage(), HttpStatus.NOT_FOUND);
        }
        if (response.getStatusCode() != HttpStatus.OK) throw new BuildITException("Error in buildIT system", response.getStatusCode());


        return null;
    }

}
