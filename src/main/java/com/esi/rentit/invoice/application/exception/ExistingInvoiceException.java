package com.esi.rentit.invoice.application.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ExistingInvoiceException extends RuntimeException {
    public ExistingInvoiceException(String message) {
        super(message);
    }
}
