package com.esi.rentit.invoice.rest;

import com.esi.rentit.invoice.application.dto.InvoiceDto;
import com.esi.rentit.invoice.application.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/invoice")
public class InvoiceRestController {

    @Autowired
    InvoiceService invoiceService;

    @GetMapping
    public ResponseEntity<List<InvoiceDto>> fetchInvoices() {
        return new ResponseEntity<>(invoiceService.fetchInvoices(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<InvoiceDto> fetchInvoice(@PathVariable Long id) {
        return new ResponseEntity<>(invoiceService.fetchInvoice(id), HttpStatus.OK);
    }

    @PutMapping("/{id}/accept")
    public ResponseEntity<InvoiceDto> acceptInvoice(@PathVariable Long id) {
        return new ResponseEntity<>(invoiceService.accept(id), HttpStatus.OK);
    }

    @PutMapping("/{id}/reject")
    public ResponseEntity<InvoiceDto> rejectInvoice(@PathVariable Long id) {
        return new ResponseEntity<>(invoiceService.reject(id), HttpStatus.OK);
    }

    @PutMapping("/{id}/reminder")
    public ResponseEntity<InvoiceDto> reminder(@PathVariable Long id) {
        return new ResponseEntity<>(invoiceService.reminder(id), HttpStatus.OK);
    }

}
