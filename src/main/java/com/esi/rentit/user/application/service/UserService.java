package com.esi.rentit.user.application.service;

import com.esi.rentit.user.application.dto.CreateUserDto;
import com.esi.rentit.user.application.exception.UserExistingException;
import com.esi.rentit.user.domain.model.RentItUser;
import com.esi.rentit.user.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.esi.rentit.user.application.service.UserValidator.validateUser;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public RentItUser createUser(CreateUserDto dto) {
        Optional<RentItUser> existing = userRepository.findRentItUserByEmailEquals(dto.getEmail());
        if (existing.isPresent()) throw new UserExistingException("User with such email already exists");
        RentItUser user = RentItUser.of(
                dto.getEmail(),
                dto.getPassword(),
                dto.getUriToSendInvoice()
        );

        validateUser(user);
        userRepository.save(user);
        return user;
    }

}
