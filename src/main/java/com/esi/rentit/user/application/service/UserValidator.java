package com.esi.rentit.user.application.service;

import com.esi.rentit.common.application.exception.InvalidBusinessPeriodException;
import com.esi.rentit.common.application.service.BusinessPeriodValidator;
import com.esi.rentit.user.application.exception.InvalidUserException;
import com.esi.rentit.user.domain.model.RentItUser;
import org.hibernate.validator.internal.constraintvalidators.bv.EmailValidator;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.validation.DataBinder;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.validation.ConstraintValidatorContext;

@Service
public class UserValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return RentItUser.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        RentItUser user = (RentItUser) target;

        if (StringUtils.isEmpty(user.getEmail())) {
            errors.rejectValue("email", "email cannot be null or empty");
        }
        if (StringUtils.isEmpty(user.getPassword())) {
            errors.rejectValue("password", "password cannot be null or empty");
        }
        if (StringUtils.isEmpty(user.getUriToSendInvoice())) {
            errors.rejectValue("uriToSendInvoice", "uriToSendInvoice cannot be null or empty");
        }

        EmailValidator validator = new EmailValidator();
        if (!validator.isValid(user.getEmail(), null)) {
            errors.rejectValue("email", "format of email is not valid");
        }
    }

    public static void validateUser(RentItUser user) {
        DataBinder binder = new DataBinder(user);
        binder.addValidators(new UserValidator());
        binder.validate();

        if (binder.getBindingResult().hasErrors()) {
            throw new InvalidUserException(binder.getBindingResult().getAllErrors().toString());
        }
    }
}
