package com.esi.rentit.user.application.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor(force = true, access = AccessLevel.PUBLIC)
@AllArgsConstructor(staticName = "of")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CreateUserDto {

    String firstName;
    String lastName;
    String email;
    String password;
    String uriToSendInvoice;

}
