package com.esi.rentit.user.domain.repository;

import com.esi.rentit.user.domain.model.RentItUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<RentItUser, Long> {

    Optional<RentItUser> findRentItUserByEmailEquals(String email);

}
