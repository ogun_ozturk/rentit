package com.esi.rentit.inventory.domain.model;

import com.esi.rentit.common.domain.BaseEntity;
import com.esi.rentit.po.domain.model.PurchaseOrder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class PlantInventoryItem extends BaseEntity {

    @Column(nullable = false)
    private String serialNumber;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private EquipmentCondition equipmentCondition;

    @ManyToOne
    @JoinColumn(name = "plant_inventory_entry_id", nullable = false)
    private PlantInventoryEntry plantInventoryEntry;

    @OneToMany(mappedBy = "plantInventoryItem", fetch = FetchType.LAZY) // no operation is cascaded, fetch lazy
    private List<PurchaseOrder> purchaseOrders;
}
