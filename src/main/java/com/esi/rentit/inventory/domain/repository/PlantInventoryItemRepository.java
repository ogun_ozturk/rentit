package com.esi.rentit.inventory.domain.repository;

import com.esi.rentit.inventory.domain.model.PlantInventoryItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.sql.Date;
import java.util.List;

public interface PlantInventoryItemRepository extends JpaRepository<PlantInventoryItem, Long> {

    String AVAILABLE_ITEMS = "" +
            "SELECT p.* " +
            "FROM plant_inventory_item p " +
            "WHERE p.plant_inventory_entry_id = :ID " +
            "AND p.equipment_condition = 'SERVICEABLE' " +
            "AND p.id NOT IN (SELECT po.plant_inventory_item_id " +
            "FROM purchase_order po " +
            "WHERE :START_DATE < po.end_date AND :END_DATE > po.start_date " +
            "AND po.status IN ('ACCEPTED', 'DISPATCHED', 'DISPATCHED_BEING_REVIEWED') " +
            ")";

    String IS_AVAILABLE = "" +
            "SELECT EXISTS(" +
            "SELECT p FROM plant_inventory_item p " +
            "WHERE p.id = :ID " +
            "AND p.equipment_condition = 'SERVICEABLE' " +
            "AND p.id NOT IN (SELECT po.plant_inventory_item_id " +
            "FROM purchase_order po " +
            "WHERE (:START_DATE < po.end_date AND :END_DATE > po.start_date) " +
            "AND po.status IN ('ACCEPTED', 'DISPATCHED') " +
            "))";

    @Query(nativeQuery = true, value = AVAILABLE_ITEMS)
    List<PlantInventoryItem> getAvailableItemsByEntryId(@Param("ID") Long id, @Param("START_DATE") Date startDate, @Param("END_DATE") Date endDate);

    @Query(nativeQuery = true, value = IS_AVAILABLE)
    boolean isAvailable(@Param("ID") Long itemId, @Param("START_DATE") Date startDate, @Param("END_DATE") Date endDate);


}
