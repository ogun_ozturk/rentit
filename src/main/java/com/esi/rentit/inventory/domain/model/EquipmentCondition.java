package com.esi.rentit.inventory.domain.model;

public enum EquipmentCondition {
    SERVICEABLE,
    NOT_SERVICEABLE
}
