package com.esi.rentit.inventory.domain.model;

import com.esi.rentit.common.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class PlantInventoryEntry extends BaseEntity {

    @Column(nullable = false)
    private String name;

    private String description;

    @Column(precision = 8, scale = 2, nullable = false)
    private BigDecimal pricePerDay;

    @OneToMany(mappedBy = "plantInventoryEntry", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<PlantInventoryItem> plantInventoryItems;
}
