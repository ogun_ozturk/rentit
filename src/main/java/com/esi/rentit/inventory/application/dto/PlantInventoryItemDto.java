package com.esi.rentit.inventory.application.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.hateoas.ResourceSupport;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor(staticName = "of")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
public class PlantInventoryItemDto extends ResourceSupport {
    Long _id;
    String serialNumber;
    String equipmentCondition;
}
