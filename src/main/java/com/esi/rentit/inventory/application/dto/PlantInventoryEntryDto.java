package com.esi.rentit.inventory.application.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.hateoas.ResourceSupport;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor(staticName = "of")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PlantInventoryEntryDto extends ResourceSupport {

    Long _id;
    String name;
    String description;
    BigDecimal pricePerDay;
    List<PlantInventoryItemDto> items;

}
