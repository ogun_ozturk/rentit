package com.esi.rentit.inventory.application.service;

import com.esi.rentit.common.application.exception.NotFoundException;
import com.esi.rentit.common.domain.BusinessPeriod;
import com.esi.rentit.inventory.application.dto.AvailableItemDto;
import com.esi.rentit.inventory.application.dto.CatalogDto;
import com.esi.rentit.inventory.application.dto.PlantInventoryEntryDto;
import com.esi.rentit.inventory.application.dto.TotalPriceDto;
import com.esi.rentit.inventory.domain.model.PlantInventoryEntry;
import com.esi.rentit.inventory.domain.model.PlantInventoryItem;
import com.esi.rentit.inventory.domain.repository.PlantInventoryEntryRepository;
import com.esi.rentit.inventory.domain.repository.PlantInventoryItemRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

import static com.esi.rentit.common.application.service.BusinessPeriodValidator.validateBusinessPeriod;

@Service
public class InventoryService {

    private final PlantInventoryEntryRepository plantInventoryEntryRepository;
    private final PlantInventoryItemRepository plantInventoryItemRepository;

    private final CatalogAssembler catalogAssembler;
    private final PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    public InventoryService(PlantInventoryEntryRepository plantInventoryEntryRepository,
                            PlantInventoryItemRepository plantInventoryItemRepository,
                            CatalogAssembler catalogAssembler,
                            PlantInventoryEntryAssembler plantInventoryEntryAssembler) {

        this.plantInventoryEntryRepository = plantInventoryEntryRepository;
        this.plantInventoryItemRepository = plantInventoryItemRepository;
        this.catalogAssembler = catalogAssembler;
        this.plantInventoryEntryAssembler = plantInventoryEntryAssembler;
    }

    public List<CatalogDto> getCatalogEntries() {

        List<PlantInventoryEntry> catalogueEntries = plantInventoryEntryRepository.findAll();

        if (catalogueEntries == null || catalogueEntries.isEmpty()) {
            throw new NotFoundException("There are no catalogue entries");
        }

        return catalogAssembler.toResources(catalogueEntries);
    }

    public PlantInventoryEntryDto getPlantInventoryEntryById(Long id) {

        Optional<PlantInventoryEntry> optionalPlantInventoryEntry = plantInventoryEntryRepository.findById(id);

        if (!optionalPlantInventoryEntry.isPresent()) {
            throw new NotFoundException("Unable to find plant inventory entry");
        }

        return plantInventoryEntryAssembler.toResource(optionalPlantInventoryEntry.get());
    }

    public TotalPriceDto getTotalPriceForIntervalByItemId(Long itemId, LocalDate startDate, LocalDate endDate) {
        BusinessPeriod rentalPeriod = BusinessPeriod.of(startDate, endDate);
        validateBusinessPeriod(rentalPeriod);

        long hiringDays = ChronoUnit.DAYS.between(startDate, endDate);

        Optional<PlantInventoryItem> optionalItem = plantInventoryItemRepository.findById(itemId);

        if (!optionalItem.isPresent()) {
            throw new NotFoundException("Unable to find plant inventory item");
        }

        PlantInventoryItem item = optionalItem.get();
        PlantInventoryEntry entry = item.getPlantInventoryEntry();

        BigDecimal pricePerDay = entry.getPricePerDay();
        BigDecimal totalPrice = pricePerDay.multiply(new BigDecimal(hiringDays));

        return TotalPriceDto.of(
                item.getId(),
                startDate,
                endDate,
                hiringDays,
                entry.getPricePerDay(),
                totalPrice
        );
    }

    public AvailableItemDto getAvailablePlantInventoryItem(Long entryId, LocalDate startDate, LocalDate endDate) {
        BusinessPeriod interval = BusinessPeriod.of(startDate, endDate);
        validateBusinessPeriod(interval);

        List<PlantInventoryItem> availableItems = plantInventoryItemRepository.getAvailableItemsByEntryId(
                entryId,
                Date.valueOf(interval.getStartDate()),
                Date.valueOf(interval.getEndDate())
        );

        if (availableItems == null || availableItems.isEmpty()) {
            throw new NotFoundException("There are no available items between the specified period");
        }

        // Return only first of the available items for possible hiring
        PlantInventoryItem assignedItem = availableItems.get(0);

        String entryName = assignedItem.getPlantInventoryEntry().getName();
        long hiringDays = ChronoUnit.DAYS.between(startDate, endDate) + 1; // add one to to endDate as a hiring day

        BigDecimal itemPricePerDay = assignedItem.getPlantInventoryEntry().getPricePerDay();
        BigDecimal totalPrice = itemPricePerDay.multiply(new BigDecimal(hiringDays));

        return AvailableItemDto.of(
                assignedItem.getId(),
                assignedItem.getSerialNumber(),
                entryName,
                assignedItem.getEquipmentCondition().name(),
                totalPrice
        );
    }
}
