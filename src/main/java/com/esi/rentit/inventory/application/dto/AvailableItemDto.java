package com.esi.rentit.inventory.application.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor(staticName = "of")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
public class AvailableItemDto {
    Long _id;
    String serialNumber;
    String entryName;
    String equipmentCondition;
    BigDecimal totalPrice;
}
