package com.esi.rentit.inventory.application.service;

import com.esi.rentit.inventory.application.dto.PlantInventoryItemDto;
import com.esi.rentit.inventory.domain.model.PlantInventoryItem;
import com.esi.rentit.inventory.rest.InventoryRestController;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class PlantInventoryItemAssembler extends ResourceAssemblerSupport<PlantInventoryItem, PlantInventoryItemDto> {

    public PlantInventoryItemAssembler() {
        super(InventoryRestController.class, PlantInventoryItemDto.class);
    }

    @Override
    public PlantInventoryItemDto toResource(PlantInventoryItem plantInventoryItem) {
        PlantInventoryItemDto dto = createResourceWithId(plantInventoryItem.getId(), plantInventoryItem);

        dto.set_id(plantInventoryItem.getId());
        dto.setSerialNumber(plantInventoryItem.getSerialNumber());
        dto.setEquipmentCondition(plantInventoryItem.getEquipmentCondition().name());

        return dto;
    }
}
