package com.esi.rentit.inventory.application.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.hateoas.ResourceSupport;

import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor(staticName = "of")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
public class CatalogDto extends ResourceSupport {
    Long _id;
    String name;
    String description;
    BigDecimal pricePerDay;
}
