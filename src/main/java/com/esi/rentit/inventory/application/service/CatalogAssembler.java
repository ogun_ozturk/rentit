package com.esi.rentit.inventory.application.service;

import com.esi.rentit.inventory.application.dto.CatalogDto;
import com.esi.rentit.inventory.domain.model.PlantInventoryEntry;
import com.esi.rentit.inventory.rest.InventoryRestController;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

@Component
public class CatalogAssembler extends ResourceAssemblerSupport<PlantInventoryEntry, CatalogDto> {

    public CatalogAssembler() {
        super(InventoryRestController.class, CatalogDto.class);
    }

    @Override
    public CatalogDto toResource(PlantInventoryEntry plantInventoryEntry) {
        CatalogDto dto = createResourceWithId(plantInventoryEntry.getId(), plantInventoryEntry);
        dto.set_id(plantInventoryEntry.getId());
        dto.setName(plantInventoryEntry.getName());
        dto.setDescription(plantInventoryEntry.getDescription());
        dto.setPricePerDay(plantInventoryEntry.getPricePerDay());
        return dto;
    }
}
