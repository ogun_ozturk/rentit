package com.esi.rentit.inventory.application.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;
import java.time.LocalDate;

@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor(staticName = "of")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
public class TotalPriceDto {
    Long plantInventoryItemId;
    LocalDate startDate;
    LocalDate endDate;
    Long hiringDays;
    BigDecimal pricePerDay;
    BigDecimal totalPrice;
}
