package com.esi.rentit.inventory.application.service;

import com.esi.rentit.inventory.application.dto.PlantInventoryEntryDto;
import com.esi.rentit.inventory.domain.model.PlantInventoryEntry;
import com.esi.rentit.inventory.rest.InventoryRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;


@Component
public class PlantInventoryEntryAssembler extends ResourceAssemblerSupport<PlantInventoryEntry, PlantInventoryEntryDto> {

    @Autowired
    PlantInventoryItemAssembler plantInventoryItemAssembler;

    public PlantInventoryEntryAssembler() {
        super(InventoryRestController.class, PlantInventoryEntryDto.class);
    }

    @Override
    public PlantInventoryEntryDto toResource(PlantInventoryEntry plantInventoryEntry) {
        PlantInventoryEntryDto dto = createResourceWithId(plantInventoryEntry.getId(), plantInventoryEntry);
        dto.set_id(plantInventoryEntry.getId());
        dto.setName(plantInventoryEntry.getName());
        dto.setDescription(plantInventoryEntry.getDescription());
        dto.setPricePerDay(plantInventoryEntry.getPricePerDay());
        dto.setItems(plantInventoryItemAssembler.toResources(plantInventoryEntry.getPlantInventoryItems()));
        return dto;
    }
}
