package com.esi.rentit.inventory.rest;

import com.esi.rentit.inventory.application.dto.AvailableItemDto;
import com.esi.rentit.inventory.application.dto.CatalogDto;
import com.esi.rentit.inventory.application.dto.PlantInventoryEntryDto;
import com.esi.rentit.inventory.application.dto.TotalPriceDto;
import com.esi.rentit.inventory.application.service.InventoryService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RequestMapping("/api/v1/inventory")
@RestController
public class InventoryRestController {

    private final InventoryService inventoryService;

    public InventoryRestController(InventoryService inventoryService) {
        this.inventoryService = inventoryService;
    }

    @CrossOrigin
    @GetMapping
    public ResponseEntity<List<CatalogDto>> getCatalogEntries() {
        return new ResponseEntity<>(inventoryService.getCatalogEntries(), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/{id}")
    public ResponseEntity<PlantInventoryEntryDto> getPlantInventoryEntryById(@PathVariable Long id) {
        return new ResponseEntity<>(inventoryService.getPlantInventoryEntryById(id), HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/available")
    public ResponseEntity<AvailableItemDto> getAvailablePlantInventoryItem(
            @RequestParam(name = "entryId") Long entryId,
            @RequestParam(name = "startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(name = "endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {

        return new ResponseEntity<>(
                inventoryService.getAvailablePlantInventoryItem(
                        entryId,
                        startDate,
                        endDate
                ),
                HttpStatus.OK
        );
    }

    @CrossOrigin
    @GetMapping("/item-total-price")
    public ResponseEntity<TotalPriceDto> getTotalPriceForIntervalByItemId(
            @RequestParam("itemId") Long itemId,
            @RequestParam("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {

        return new ResponseEntity<>(
                inventoryService.getTotalPriceForIntervalByItemId(
                        itemId,
                        startDate,
                        endDate
                ),
                HttpStatus.OK
        );
    }
}
