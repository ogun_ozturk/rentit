package com.esi.rentit.common.rest;

import com.esi.rentit.common.application.dto.ErrorDetailDto;
import com.esi.rentit.common.application.exception.BuildITException;
import com.esi.rentit.common.application.exception.InvalidBusinessPeriodException;
import com.esi.rentit.common.application.exception.NotFoundException;
import com.esi.rentit.invoice.application.exception.ExistingInvoiceException;
import com.esi.rentit.invoice.application.exception.InvoiceIllegalStateException;
import com.esi.rentit.po.application.exception.ItemNotAvailableException;
import com.esi.rentit.user.application.exception.InvalidUserException;
import com.esi.rentit.user.application.exception.UserExistingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@RestControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    private Logger logger = LoggerFactory.getLogger(CustomExceptionHandler.class);

    @ExceptionHandler(NotFoundException.class)
    public final ResponseEntity<ErrorDetailDto> handlePoNotFoundException(NotFoundException ex, WebRequest request) {
        return returnResponseEntity(ex, request, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ItemNotAvailableException.class)
    public final ResponseEntity<ErrorDetailDto> handleItemNotAvailableException(ItemNotAvailableException ex, WebRequest request) {
        return returnResponseEntity(ex, request, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(InvalidBusinessPeriodException.class)
    public final ResponseEntity<ErrorDetailDto> handleInvalidBusinessPeriodException(InvalidBusinessPeriodException ex, WebRequest request) {
        return returnResponseEntity(ex, request, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(InvalidUserException.class)
    public final ResponseEntity<ErrorDetailDto> handleInvalidUserExceptionException(InvalidUserException ex, WebRequest request) {
        return returnResponseEntity(ex, request, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserExistingException.class)
    public final ResponseEntity<ErrorDetailDto> handleUserExistingException(UserExistingException ex, WebRequest request) {
        return returnResponseEntity(ex, request, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvoiceIllegalStateException.class)
    public final ResponseEntity<ErrorDetailDto> handleInvoiceIllegalStateException(InvoiceIllegalStateException ex, WebRequest request) {
        return returnResponseEntity(ex, request, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(BuildITException.class)
    public final ResponseEntity<ErrorDetailDto> handleBuildITException(BuildITException ex, WebRequest request) {
        return returnResponseEntity(ex, request, ex.getStatus());
    }

    @ExceptionHandler(ExistingInvoiceException.class)
    public final ResponseEntity<ErrorDetailDto> handleExistingInvoiceException(ExistingInvoiceException ex, WebRequest request) {
        return returnResponseEntity(ex, request, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handle(Exception ex, WebRequest request) {

        logger.error(ex.getMessage(), ex);

        ErrorDetailDto errorDetailDto = ErrorDetailDto.of(
                LocalDateTime.now(),
                "Ups!Something went wrong",
                request.getDescription(false)
        );

        return new ResponseEntity<>(errorDetailDto, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<ErrorDetailDto> returnResponseEntity(RuntimeException ex, WebRequest request, HttpStatus httpStatus) {
        logger.error(ex.getMessage(), ex);

        ErrorDetailDto errorDetailDto = ErrorDetailDto.of(
                LocalDateTime.now(),
                ex.getMessage(),
                request.getDescription(false)
        );

        return new ResponseEntity<>(errorDetailDto, httpStatus);
    }
}
