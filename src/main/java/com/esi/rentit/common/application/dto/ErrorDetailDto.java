package com.esi.rentit.common.application.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor(staticName = "of")
@Data
public class ErrorDetailDto {
    private LocalDateTime dateTime;
    private String message;
    private String details;
}
