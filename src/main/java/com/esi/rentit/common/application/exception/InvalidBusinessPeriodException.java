package com.esi.rentit.common.application.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class InvalidBusinessPeriodException extends RuntimeException {
    public InvalidBusinessPeriodException(String message) {
        super(message);
    }
}
