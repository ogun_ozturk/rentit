package com.esi.rentit.common.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.persistence.Embeddable;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Embeddable
@Value
@NoArgsConstructor(force = true, access = AccessLevel.PUBLIC)
@AllArgsConstructor(staticName = "of")
public class BusinessPeriod {
    private LocalDate startDate;
    private LocalDate endDate;

    public Long durationInDays() {
        return ChronoUnit.DAYS.between(startDate, endDate);
    }
}
