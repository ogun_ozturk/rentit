

INSERT INTO plant_inventory_entry (name, created_at, updated_at, description, price_per_day) VALUES
('Mini excavator Yanmar 31NV70 300.9D', current_timestamp, current_timestamp , 'net power: 12.0 HP, weight: 2061.0 lb', 10.12),
('Mini excavator Yanmar 31NV76 301.4D', current_timestamp, current_timestamp , 'net power: 17.7 HP, weight: 3241.0 lb', 20.22),
('Mini excavator C1.1 31NV70 301.7CR', current_timestamp, current_timestamp , 'net power: 19.2 HP, weight: 4045.0 lb', 33.12),
('Backhoe loader 420F/420F IT', current_timestamp, current_timestamp , 'net power: 70.0 kW, dig depth: 4360.0 mm, weight: 11000.0 kg', 50.12),
('Backhoe loader 432F', current_timestamp, current_timestamp , 'net power: 70.9 kW, weight: 8479.0 kg', 45.16),
('Small Dozer D3K2 TIER 4 FINAL', current_timestamp, current_timestamp, 'net power: 80.0 HP, weight: 17465.0 lb', 70.25),
('Large Dozer D9T', current_timestamp, current_timestamp, 'net power: 436.0 HP, weight: 106618.0 lb', 100.25);

INSERT INTO plant_inventory_item (created_at, updated_at, equipment_condition, serial_number, plant_inventory_entry_id) VALUES
(current_timestamp, current_timestamp, 'SERVICEABLE', 'AfIWlx1tdnqwOa2K', 1),
(current_timestamp, current_timestamp, 'SERVICEABLE', '8481w1PgThUXAjN1', 1),
(current_timestamp, current_timestamp, 'NOT_SERVICEABLE', '93kXn5YZ3RTJ7X1N', 1),
(current_timestamp, current_timestamp, 'NOT_SERVICEABLE', 'SwuFMrTGQXqB6Xal', 1),
(current_timestamp, current_timestamp, 'SERVICEABLE', '05iHZq6hACb4mAnM', 2),
(current_timestamp, current_timestamp, 'SERVICEABLE', 'fsjnLm36HVVKqH5a', 2),
(current_timestamp, current_timestamp, 'NOT_SERVICEABLE', 'XkfiSMWPrlTJoK7r', 2),
(current_timestamp, current_timestamp, 'NOT_SERVICEABLE', 'U5UDQXZG35zRGP3W', 2),
(current_timestamp, current_timestamp, 'SERVICEABLE', '6SOVMTz8ixclpwaH', 3),
(current_timestamp, current_timestamp, 'SERVICEABLE', 'gFHwfKee31r1woBY', 3),
(current_timestamp, current_timestamp, 'NOT_SERVICEABLE', 'myRrbGFTfhv4iIZL', 3),
(current_timestamp, current_timestamp, 'NOT_SERVICEABLE', 'QdfEpRopq662p02d', 3),
(current_timestamp, current_timestamp, 'SERVICEABLE', 'Fq3emkBuHThqqEti', 4),
(current_timestamp, current_timestamp, 'SERVICEABLE', 'gaWxuSLgtyR1WxSy', 4),
(current_timestamp, current_timestamp, 'NOT_SERVICEABLE', 'LXrC8Lvu8bF2L1oz', 4),
(current_timestamp, current_timestamp, 'NOT_SERVICEABLE', 'W982PL85gpkJi4yb', 4),
(current_timestamp, current_timestamp, 'SERVICEABLE', 'mMIfo0CBaVe2asGK', 5),
(current_timestamp, current_timestamp, 'SERVICEABLE', 'fGYrk6pH7WuEqi0e', 5),
(current_timestamp, current_timestamp, 'NOT_SERVICEABLE', '8DWFwuwV55diqr2Z', 5),
(current_timestamp, current_timestamp, 'NOT_SERVICEABLE', 'G5OTpqXTItrCYEpx', 5),
(current_timestamp, current_timestamp, 'SERVICEABLE', 'q49kDHkZN7ZoJaHv', 6),
(current_timestamp, current_timestamp, 'SERVICEABLE', '0qr4lYZCAfboS8mn', 6),
(current_timestamp, current_timestamp, 'NOT_SERVICEABLE', '4R1YBneDA12Zk1tP', 6),
(current_timestamp, current_timestamp, 'NOT_SERVICEABLE', 'uhxqpDsGAExdGp3n', 6),
(current_timestamp, current_timestamp, 'SERVICEABLE', 'WT6Cf2KYhcwbAMEy', 7),
(current_timestamp, current_timestamp, 'SERVICEABLE', 'ChyA0pNpwrChLX21', 7),
(current_timestamp, current_timestamp, 'NOT_SERVICEABLE', 'RtNbctQJ9UIUIKoX', 7),
(current_timestamp, current_timestamp, 'NOT_SERVICEABLE', 'KDuBaqEQ2m99QgNY', 7);

INSERT INTO rent_it_user (created_at, updated_at, email, password, uri_to_send_invoice) VALUES
(current_timestamp, current_timestamp, 'our@buildit.org', 'notpwd123', 'https://young-garden-17558.herokuapp.com/api/v1/invoice');
